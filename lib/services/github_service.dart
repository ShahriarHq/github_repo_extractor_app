import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/repository.dart';

class GithubService {
  static const String baseUrl = 'https://api.github.com/search/repositories';
  static const String query = 'Flutter';

  static Future<List<Repository>> fetchRepositories(int page) async {
    try {
      final response =
          await http.get(Uri.parse('$baseUrl?q=$query&page=$page'));

      print('Printing Status code: ${response.body}');

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = json.decode(response.body);
        final List<dynamic> items = data['items'];

        return items
            .map((item) => Repository(
                  id: item['id'],
                  name: item['name'],
                  owner: item['owner']['login'],
                  description: item['description'] ?? '',
                  lastUpdated: DateTime.parse(item['updated_at']),
                  stars: item['stargazers_count'],
                  ownerAvatarUrl: item['owner']['avatar_url'],
                ))
            .toList();
      } else {
        throw Exception('Failed to load repositories');
      }
    } catch (error) {
      print('Error fetching repositories: $error');
      throw Exception('Failed to get repositories');
    }
  }
}

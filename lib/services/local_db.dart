import 'package:hive/hive.dart';
import '../models/repository.dart';

class LocalDatabase {
  static const String boxName = 'repositories';

  static Future<void> saveRepositories(List<Repository> repositories) async {
    try {
      final box = await Hive.openBox<Repository>(boxName);
      await box.clear();
      await box.addAll(repositories);
    } catch (e) {
      print('Error saving repositories: $e');
    }
  }

  static Future<List<Repository>> getRepositories() async {
    final box = await Hive.openBox(boxName);
    return List<Repository>.from(box.values);
  }
}
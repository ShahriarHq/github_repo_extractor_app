import 'package:flutter/material.dart';
import '../models/repository.dart';
import '../screens/repo_details_screen.dart';

class RepositoryItem extends StatelessWidget {
  final Repository repository;

  const RepositoryItem({Key? key, required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(repository.name, style: TextStyle(fontWeight: FontWeight.w900,color: Colors.amber),),
      subtitle: Text(repository.owner),
      trailing: Text('${repository.stars} Stars'),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RepositoryDetailsScreen(repository: repository),
          ),
        );
      },
    );
  }
}

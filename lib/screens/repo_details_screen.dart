import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import '../models/repository.dart';
import 'package:intl/intl.dart';

class RepositoryDetailsScreen extends StatelessWidget {
  final Repository repository;

  const RepositoryDetailsScreen({Key? key, required this.repository})
      : super(key: key);

Future<void> _saveDetailsOffline() async {
  try{
    final box = await Hive.openBox<RepositoryDetails>('repository_details');
    final repositoryDetails = RepositoryDetails(
      owner: repository.owner,
      ownerAvatarUrl: repository.ownerAvatarUrl,
      description: repository.description,
      lastUpdated: repository.lastUpdated,
    );
    await box.put(repository.id, repositoryDetails);
  } catch (e){
    print('Error saving repositories in product details: $e');
  }
    
  }

  @override
  Widget build(BuildContext context) {
    _saveDetailsOffline();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        title: Text('Repository Details'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(repository.owner.toUpperCase(),
              style: const TextStyle(fontSize:20, fontWeight: FontWeight.w900, color: Colors.amber),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ClipOval(
              child: Image.network(
                repository.ownerAvatarUrl,
                width: 200,
                height: 200,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Description: ${repository.description}',
              style: TextStyle(fontSize: 18),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Last Updated: ${DateFormat('MM-dd-yyyy HH:mm').format(repository.lastUpdated)}',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}


// Create a custom object to store all details for offline browsing
@HiveType(typeId: 1)
class RepositoryDetails {
  @HiveField(0)
  late String owner;

  @HiveField(1)
  late String ownerAvatarUrl;

  @HiveField(2)
  late String description;

  @HiveField(3)
  late DateTime lastUpdated;

  RepositoryDetails({
    required this.owner,
    required this.ownerAvatarUrl,
    required this.description,
    required this.lastUpdated,
  });
}

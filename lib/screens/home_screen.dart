import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import '../models/repository.dart';
import '../services/github_service.dart';
import '../services/local_db.dart';
import '../widgets/repository_item.dart';
import 'package:slidable_button/slidable_button.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late List<Repository> repositories;
  late ScrollController _scrollController;
  bool sortAscending = false;
  bool isLoading = false;
  DateTime lastFetchTime = DateTime(2000);

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    repositories = [];
    _fetchRepositories();
    _initHive();
    _loadCachedData();
  }

  Future<void> _initHive() async {
    final appDocumentDirectory = await getApplicationDocumentsDirectory();
    Hive.init(appDocumentDirectory.path);
    await Hive.openBox<Repository>('repositories');
  }

  Future<void> _loadCachedData() async {
    await Hive.box('repositories').close();
    final box = await Hive.openBox<Repository>('repositories');
    final cachedRepositories = box.values.toList();
    setState(() {
      repositories.addAll(cachedRepositories);
    });
  }

  void _sortRepositories() {
    repositories.sort((a, b) => sortAscending
        ? a.stars.compareTo(b.stars)
        : b.stars.compareTo(a.stars));
  }

  void _sortRepositoriesByDateTime() {
    repositories.sort((a, b) => sortAscending
        ? a.lastUpdated.compareTo(b.lastUpdated)
        : b.lastUpdated.compareTo(a.lastUpdated));
  }

  Future<void> _fetchRepositories() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      final newRepositories =
          await GithubService.fetchRepositories(repositories.length ~/ 10 + 1);

      setState(() {
        repositories.addAll(newRepositories);
        _sortRepositories();
        isLoading = false;
      });

      await LocalDatabase.saveRepositories(repositories);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        title: const Text('Git Repositories'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: repositories.length,
              itemBuilder: (context, index) {
                if (index.isOdd) {
                  if (index == repositories.length - 1) {
                  _scrollController.addListener(() {
                    if (_scrollController.position.pixels ==
                        _scrollController.position.maxScrollExtent) {
                      _fetchRepositories();
                    }
                  });
                }
                  return const Divider(
                    height: 1,
                    color: Colors.amberAccent,
                  );
                }else{
                  if (index == repositories.length - 1) {
                  _scrollController.addListener(() {
                    if (_scrollController.position.pixels ==
                        _scrollController.position.maxScrollExtent) {
                      _fetchRepositories();
                    }
                  });
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 8.0),
                  child: RepositoryItem(repository: repositories[index]),
                );
                }
              },
              controller: _scrollController,
            ),
          ),
          if (isLoading)
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.all(16.0),
              child: const CircularProgressIndicator(),
            ),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.width / 7,
          ),
          HorizontalSlidableButton(
            width: MediaQuery.of(context).size.width / 2.5,
            buttonWidth: MediaQuery.of(context).size.width / 4.5,
            color: Theme.of(context).colorScheme.secondary.withOpacity(0.5),
            buttonColor: Theme.of(context).primaryColorDark,
            dismissible: false,
            label: const Center(child: Text('Slide To Sort')),
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('On Star'),
                  Text('On Time'),
                ],
              ),
            ),
            onChanged: (position) {
              setState(() {
                if (position == SlidableButtonPosition.end) {
                  _sortRepositoriesByDateTime();
                } else {
                  _sortRepositories();
                }
              });
            },
          ),
        ],
      ),
    );
  }
}

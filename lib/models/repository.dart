// class Repository {
//   final int id;
//   final String name;
//   final String owner;
//   final String description;
//   final DateTime lastUpdated;
//   final int stars;
//   final String ownerAvatarUrl;

//   Repository({
//     required this.id,
//     required this.name,
//     required this.owner,
//     required this.description,
//     required this.lastUpdated,
//     required this.stars,
//     required this.ownerAvatarUrl,
//   });
// }

class Repository {
  final int id;
  final String name;
  final String owner;
  final String description;
  final DateTime lastUpdated;
  final int stars;
  final String ownerAvatarUrl;

  Repository({
    required this.id,
    required this.name,
    required this.owner,
    required this.description,
    required this.lastUpdated,
    required this.stars,
    required this.ownerAvatarUrl,
  });

  // Factory constructor to create a Repository instance from a Map
  factory Repository.fromJson(Map<dynamic, dynamic> json) {
    return Repository(
      id: json['id'] as int,
      name: json['name'] as String,
      owner: json['owner'] as String,
      description: json['description'] as String,
      lastUpdated: DateTime.parse(json['lastUpdated'] as String),
      stars: json['stars'] as int,
      ownerAvatarUrl: json['ownerAvatarUrl'] as String,
    );
  }

  // Method to convert a Repository instance to a Map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'owner': owner,
      'description': description,
      'lastUpdated': lastUpdated.toIso8601String(),
      'stars': stars,
      'ownerAvatarUrl': ownerAvatarUrl,
    };
  }
}

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'screens/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory = await getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  await Hive.openBox('repositories');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter GitHub Repositories',
      theme: ThemeData(
        primarySwatch: Colors.green, // Set the primary color to green
      ),
      debugShowCheckedModeBanner: false, // Remove the debug tag
      home: HomeScreen(),
    );
  }
}

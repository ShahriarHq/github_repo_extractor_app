# github_repo_extractor_app

**===================== Flutter GitHub Repository Extractor =================**

*Overview:*

The "Flutter GitHub Repository Extractor" is a sophisticated Flutter application designed to showcase the most starred GitHub repositories related to the Flutter framework. Leveraging the power of Flutter, this application provides users with a streamlined interface to explore and discover top-rated repositories.

*Key Features:*

1. **GitHub Integration:** Seamlessly integrates with the GitHub API to fetch and display the most starred repositories.

2. **User-Friendly Interface:** Boasts an intuitive and user-friendly interface for effortless navigation and exploration of repositories.

3. **Sorting Functionality:** Enables users to sort repositories based on star count and last update time, providing a customized viewing experience.

4. **Pagination:** Implements a paginated scrolling mechanism, allowing users to fetch additional repositories as they explore the app.

5. **Offline Browsing Support:** Persists fetched repository data for offline browsing, ensuring a consistent and reliable user experience.

6. **Splash Screen:** Features an aesthetically pleasing splash screen for a polished and professional app launch experience.

*Technological Stack:*

- **Flutter:** Utilizes the Flutter framework for building a cross-platform and visually appealing user interface.

- **GitHub API:** Leverages the GitHub API for real-time data retrieval, ensuring up-to-date repository information.

- **Hive Database:** Implements Hive for efficient and secure local storage, enabling offline repository access.

*Usage:*

Users can simply launch the application to explore a curated list of the most starred GitHub repositories related to Flutter. With sorting options, pagination, and offline browsing capabilities, it offers a comprehensive and enjoyable experience for developers and Flutter enthusiasts.

*Note:*

For optimal performance, the app adheres to GitHub's data refresh policy, ensuring that repository data is refreshed no more frequently than once every 30 minutes.


![Splash Screen](assets/image/splashscreen.png "Splash Screen")

![List View Screen](assets/image/listview.jpeg "Splash Screen")

![Details Screen](assets/image/details.jpeg "Splash Screen")



**=================== Diagram of Project Architecture ==================**

**Flutter Application**

HomeScreen     

- Repository List
- Sorting Options
- Pagination     
- Loading Indicator


**Services**


 GitHub Service
 - Fetch GitHub API   

 Local Database
 - Save Repositories  
 - Load Repositories  




**Widgets**

 Repository Item      
 - Display Repository 
 - Navigate to Details 


 HorizontalSlidable   
 - Sorting UI   


**RepositoryDetailsScreen**

Details Screen  
- Owner Details  
- Avatar Image   
- Description    
- Last Updated   




**=================== Sketch ==================**
In this sketch:

- Flutter Application: The main application component that holds different screens and widgets.

- HomeScreen: The main screen displaying the list of repositories with sorting options, pagination, and a loading indicator.

- Services: The business logic layer, including the GitHub Service for API calls and Local Database for storing and retrieving repositories.

- Widgets: Reusable UI components such as Repository Item for displaying a single repository and HorizontalSlidable for sorting options.

- RepositoryDetailsScreen: A separate screen for displaying detailed information about a selected repository.


**========================= FUTURE SCOPE ===========================**

The future scope for a GitHub repositories Flutter app can include various features and improvements to enhance the user experience and functionality. Here are some potential future enhancements:

- Integrate user authentication to allow users to log in with their GitHub accounts.
- Provide personalized repository recommendations based on users' GitHub activity.
- Enhance sorting options based on different criteria.
- Implement advanced filtering options to help users find repositories more efficiently.
- Add search field to search with different keys.
- Can add feature to download the repositories.


**====================== Unfinished Works ========================**

- The fetched data should be stored in a local database to permit the app to be used in
offline. After turning off the data connection list is not visible form the local storage.
somehow the data is not saved in the local database. for this I'm using hive.


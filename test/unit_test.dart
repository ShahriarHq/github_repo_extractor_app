import 'package:flutter_test/flutter_test.dart';
import 'package:github_repo_extractor_app/services/github_service.dart';

void main() {
  group('GithubService', () {
    test('fetchRepositories returns a non-empty list', () async {
      final repositories = await GithubService.fetchRepositories(1);
      expect(repositories, isNotEmpty);
    });
  });
}

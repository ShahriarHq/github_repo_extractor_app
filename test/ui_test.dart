import 'package:flutter/material.dart';
import 'package:github_repo_extractor_app/widgets/repository_item.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:github_repo_extractor_app/main.dart';
import 'package:github_repo_extractor_app/models/repository.dart';
import 'package:github_repo_extractor_app/screens/repo_details_screen.dart';

void main() {
  testWidgets('HomeScreen UI test', (WidgetTester tester) async {

    // Mock Hive
    final appDocumentDirectory = await getApplicationDocumentsDirectory();
    Hive.init(appDocumentDirectory.path);
    await Hive.openBox<Repository>('repositories');

    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that the HomeScreen title is displayed.
    expect(find.text('Git Repositories'), findsOneWidget);

    // Wait for the repositories to be loaded
    await tester.pumpAndSettle();

    // Verify that the repositories are displayed
    expect(find.byType(RepositoryItem), findsNWidgets(10));

    // Trigger a scroll to fetch more repositories
    await tester.drag(find.byType(ListView), const Offset(0.0, -500.0));
    await tester.pumpAndSettle();

    // Verify that the repositories are updated after scrolling
    expect(find.byType(RepositoryItem), findsNWidgets(10));

    // Verify that the sort button works
    await tester.tap(find.text('Slide To Sort'));
    await tester.pumpAndSettle();

    // Verify that the repositories are sorted
    expect(find.byType(RepositoryItem), findsNWidgets(10));


  });
  testWidgets('RepositoryDetailsScreen UI test', (WidgetTester tester) async {

    // Mock repository data
    final repository = Repository(
      id: 1,
      name: 'John Doe',
      owner: 'John Doe',
      stars: 567,
      ownerAvatarUrl: 'https://example.com/avatar.jpg',
      description: 'Sample description',
      lastUpdated: DateTime.now(),
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(
      MaterialApp(
        home: RepositoryDetailsScreen(repository: repository),
      ),
    );

    // Verify that the RepositoryDetailsScreen is displayed.
    expect(find.text('Repository Details'), findsOneWidget);
    expect(find.text('alibaba'), findsOneWidget); 
    expect(find.text('Last Update'), findsOneWidget); 

  });
}
